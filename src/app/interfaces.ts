
export interface IMessage {
    msg: string;
    isImportant: boolean;
    sender: string;
}