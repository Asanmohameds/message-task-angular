import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IMessage } from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor() { }

  // creating new object from Behaviour Subject RXJS syntax

   recieveMsg:BehaviorSubject<any> = new BehaviorSubject<any>(null)

   
   callBehaviourSubject(value:IMessage) {
     debugger;
     this.recieveMsg.next(value);
   }


}
