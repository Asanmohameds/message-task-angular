import { Component, OnInit } from '@angular/core';
import { IMessage } from '../interfaces';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-all-messages',
  templateUrl: './all-messages.component.html',
  styleUrls: ['./all-messages.component.css']
})
export class AllMessagesComponent implements OnInit {

  constructor(private msgService: MessageService) { }

  // currentAllMessages = [{msg:null, checkBox:null, sender:null}]
  currentAllMessages:IMessage[] = [];

  ngOnInit(): void {

    this.msgService.recieveMsg.subscribe((x: IMessage) => {
      debugger;
      if (x) this.currentAllMessages.push(x);
      console.log(x);
    });

  }

}
