import { Component, Input, OnInit } from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {MessageService} from '../services/message.service';
import {IMessage} from '../interfaces'

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  
  @Input() Sender: string = '';

  constructor(private fb: FormBuilder, private msgService: MessageService) {}

  ngOnInit(): void {
    this.msgService.recieveMsg.subscribe((x: IMessage) => {
      if (x) {
        this.currentStrangerMsg = x;
      }
    });
  }

  // Form Value...

  messageForm = this.fb.group({
    strangerMessage: [''],
    strangerCheckBox: [false],
  });

  // Getter methods...
  get strangerMessage() {
    return this.messageForm.get('strangerMessage');
  }

  get strangerCheckBox() {
    return this.messageForm.get('strangerCheckBox');
  }

  //define Initial form value..

  currentStrangerMsg: IMessage = {
    isImportant: false,
    msg: '',
    sender: '',
  };

  sendMessage() {
    debugger;

    let obj: IMessage = {
      msg: this.strangerMessage?.value,
      isImportant: this.strangerCheckBox?.value,
      sender: this.Sender,
    };

    this.msgService.callBehaviourSubject(obj);
  }
}

