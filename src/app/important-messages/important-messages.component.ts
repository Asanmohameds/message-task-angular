import { Component, OnInit } from '@angular/core';
import { IMessage } from '../interfaces';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-important-messages',
  templateUrl: './important-messages.component.html',
  styleUrls: ['./important-messages.component.css']
})
export class ImportantMessagesComponent implements OnInit {

  constructor(private msgService:MessageService) { }

  currentImportantMsg:IMessage[]= [];

  ngOnInit(): void {

    this.msgService.recieveMsg.subscribe((x:IMessage) => {
      if(x && x.isImportant) {
        this.currentImportantMsg.push(x)
      }
    })

  }

}
